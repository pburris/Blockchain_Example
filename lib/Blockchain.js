const { hashObject, hashWord } = require('./hash');

/**
 * Node
 *
 * @description A single node on the blockchain consisting of a sender, receiver and an amount
 * @param {Object} tx, the object containing the transaction data
 * @property {Object} transaction, the transaction data
 * @property {String} index, the hashed transaction data
 * @property {Node} previous, this is the previous node, created will null
 */
function Node(tx) {
  this.transaction = tx;
  this.index = hashObject(tx);
  this.previous = null;
}

/**
 * Blockchain
 *
 * @description A chain of transactions that have a running hash value
 * @property {Node} root, root node or genesis block
 * @property {Array} indices, array of hashed nodes in order
 * @property {Array} transactions, array of raw transaction data
 * @property {String} value, the hashed value of the total of all transaction indexes
 * @property {Number} length, the amount of nodes
 */
function Blockchain() {
  const rootVal = {
    sender: 'GENESIS',
    receiver: 'GENESIS',
    amount: 0,
  };
  this.root = new Node(rootVal);
  this.indices = [this.root];
  this.transactions = [rootVal];
  this.value = hashWord(this.transactions.join(''));
  this.length = 1;

  this.updateValue = () => {
    this.value = hashWord(this.indices.join(''));
  }

  this.push = (newVal) => {
    let newNode = new Node(newVal);
    newNode.previous = this.indices[this.indices.length - 1];
    this.transactions.push(newVal);
    this.indices.push(newNode);
    this.updateValue();
    this.length += 1;
  }

  this.pop = () => {
    if (!this.length) return null;
    ret = this.transactions.pop();
    this.indices.pop();
    this.updateValue();
    this.length -= 1;
    return ret;
  }
}


module.exports = Blockchain;
