# Basic Blockchain Example


### What is a Blockchain?

A blockchain is similar to a linked-list but linked with hashed values and the entire blockchain is hashed as values are added and taken off.
I am using the sha512 algorithm to hash the incoming nodes. The blockchain itself has a value made from hashing a string made from all of the node's hashed values.

In this blockchain there is a small amount of data being stored: Sender, Receiver and Amount. This is a very loose example.


### Running and Testing

To run the example just clone this repository and run `npm start`

To test the blockchain install ava globally with `npm i -g ava` or run it from the local folder with `npm install` and finally `npm test`


### Contribute

Feel free to put in pull requests to add more to this blockchain
