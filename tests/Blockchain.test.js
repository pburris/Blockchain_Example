import test from 'ava';
import Blockchain from '../lib/Blockchain';


let genTx = () => ({
  sender: 'Person',
  receiver: 'Person2',
  amount: 14,
})


test('Genesis blocks always match', t => {
  let bc1 = new Blockchain();
  let bc2 = new Blockchain();
  t.true(bc1.value === bc2.value);
});


test('Push works', t => {
  let bc = new Blockchain();
  let tx = genTx();
  bc.push(tx);
  t.true(bc.transactions.length === 2);
})

test('Pop works', t => {
  let bc = new Blockchain();
  let tx = genTx();
  bc.push(tx);
  t.deepEqual(tx, bc.pop());
})

test('Length property is working', t => {
  let bc = new Blockchain();
  for (let i = 1; i < 50; i += 1) {
    bc.push({
      sender: 'Larry',
      receiver: 'Mary',
      amount: i,
    });
  }
  t.true(bc.length === 50);
})
