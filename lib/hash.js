const crypto = require('crypto');


/**
 * Hash Word
 *
 * @description Hash a word using sha512 and encode it in base64
 * @param {String} string, the string to hash
 * @return {String} the hashed string
 */
const hashWord = string => crypto.createHash('sha512').update(string).digest('base64');

/**
 * Hash Object
 *
 * @description Hash an object by turning it into a string and removing the spaces
 * @param {Object|Array} obj, the object to be hashed
 * @return {String} to hashed object
 */
const hashObject = obj =>
  hashWord(JSON.stringify(obj)
    .split(' ')
    .filter(letter => !(letter === ' '))
    .reduce((sum, val) => hashWord(val), ''));


module.exports = { hashWord, hashObject };
